﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.BU
{
    public class Parent
    {
        // Fields
        private int ID;
        private string name;
        private string situation;
        private string job;
        private string education;
        private string postal;

        // Constructor
        public Parent(int ID,
                        string name,
                        string situation,
                        string job,
                        string education
                        )
        {
            this.ID = ID;
            this.name = name;
            this.situation = situation;
            this.job = job;
            this.education = education;     
        }

        public Parent(string name,
                string situation,
                string job,
                string education)
        {
            this.name = name;
            this.situation = situation;
            this.job = job;
            this.education = education;
        }

        // Encapsulation
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string Situation
        {
            get
            {
                return situation;
            }

            set
            {
                situation = value;
            }
        }
        public string Job
        {
            get
            {
                return job;
            }

            set
            {
                job = value;
            }
        }
        public string Education
        {
            get
            {
                return education;
            }

            set
            {
                education = value;
            }
        }
        public int Id
        {
            get
            {
                return ID;
            }

            set
            {
                this.ID = value;
            }
        }
    }
}
