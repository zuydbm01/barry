﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.BU
{
    public class Child
    {
        // Fields
        private string name;
        private int age;
        private string sortHandicapOne;
        private string sortHandicapTwo;
        private string sortHandicapThree;
        private string specHandicapOne;
        private string specHandicapTwo;
        private string specHandicapThree;

        // Constructor
        public Child(string name, 
                        int age,
                        string sortHandicapOne,
                        string specHandicapOne,
                        string sortHandicapTwo = "",
                        string specHandicapTwo = "",
                        string sortHandicapThree = "",
                        string specHandicapThree = "")
        {
            this.name = name;
            this.age = age;
            this.SortHandicapOne = sortHandicapOne;
            this.SpecHandicapOne = specHandicapOne;
            this.SortHandicapTwo = sortHandicapTwo;
            this.SpecHandicapTwo = specHandicapTwo;
            this.SortHandicapThree = sortHandicapThree;
            this.SpecHandicapThree = specHandicapThree;

        }

        // Encapsulation
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }
        public string SortHandicapOne
        {
            get
            {
                return sortHandicapOne;
            }

            set
            {
                sortHandicapOne = value;
            }
        }
        public string SortHandicapTwo
        {
            get
            {
                return sortHandicapTwo;
            }

            set
            {
                sortHandicapTwo = value;
            }
        }
        public string SortHandicapThree
        {
            get
            {
                return sortHandicapThree;
            }

            set
            {
                sortHandicapThree = value;
            }
        }
        public string SpecHandicapOne
        {
            get
            {
                return specHandicapOne;
            }

            set
            {
                specHandicapOne = value;
            }
        }
        public string SpecHandicapTwo
        {
            get
            {
                return specHandicapTwo;
            }

            set
            {
                specHandicapTwo = value;
            }
        }
        public string SpecHandicapThree
        {
            get
            {
                return specHandicapThree;
            }

            set
            {
                specHandicapThree = value;
            }
        }
    }
}
