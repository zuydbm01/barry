﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.BU
{
    public class History
    {
        // Fields
        private string question;
        private string answer;
        private string parent;
        private string child;

        // Constructor
        public History(string question,
                        string answer,
                        string parentName,
                        string childName)
        {
            this.question = question;
            this.answer = answer;
            this.parent = parentName;
            this.child = childName;
        }

        // Encapsulations
        public string Question
        {
            get
            {
                return question;
            }

            set
            {
                question = value;
            }
        }

        public string Answer
        {
            get
            {
                return answer;
            }

            set
            {
                answer = value;
            }
        }

        public string Parent
        {
            get
            {
                return parent;
            }

            set
            {
                parent = value;
            }
        }

        public string Child
        {
            get
            {
                return child;
            }

            set
            {
                child = value;
            }
        }
    }
}
