﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.DAL
{
    public static class InternalDB
    {
        private static string acc = "schoudersAdmin";
        private static string pass = "BM01Schouders";
        
        private static string conn = $"{Properties.Settings.Default.dbConn}; User ID = {acc}; Password={pass}";
        private static SqlConnection connToInternal = new SqlConnection(conn);
        public static DataTable readDatabase(string query)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd;
            cmd = new SqlCommand(query, connToInternal);

            // Try to retrieve the data from the database.
            try
            {
                connToInternal.Open();
                dt.Load(cmd.ExecuteReader());
                return dt;
            }
            catch
            {
                // Data hasn't succesfully been retrieved to the database.
                return dt;
            }
            finally
            {
                // Always close the database connection.
                connToInternal.Close();
            }
        }

        public static bool modifyDatabase(string query)
        {
            SqlCommand cmd = new SqlCommand(query, connToInternal);

            // Try to add the data to the database.
            try
            {
                connToInternal.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {                
                // Data hasn't succesfully been written to the database.
                return false;
            }
            finally
            {
                // Always close the database connection.
                connToInternal.Close();
            }
        }
    }
}
