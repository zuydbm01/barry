﻿namespace VoorElkaarv2
{
    partial class VoorElkaar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.vePanel = new System.Windows.Forms.TabControl();
            this.questionTab = new System.Windows.Forms.TabPage();
            this.cbChildThree = new System.Windows.Forms.CheckBox();
            this.cbChildTwo = new System.Windows.Forms.CheckBox();
            this.cbChildOne = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.profileTab = new System.Windows.Forms.TabPage();
            this.btnDelChild = new System.Windows.Forms.Button();
            this.btnDelAccount = new System.Windows.Forms.Button();
            this.lblBackupName = new System.Windows.Forms.Label();
            this.btnNewChild = new System.Windows.Forms.Button();
            this.lblFaultCodeProfile = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnPreviousChild = new System.Windows.Forms.Button();
            this.btnNextChild = new System.Windows.Forms.Button();
            this.btnSaveChild = new System.Windows.Forms.Button();
            this.tbSpecHandicapThree = new System.Windows.Forms.TextBox();
            this.tbSpecHandicapTwo = new System.Windows.Forms.TextBox();
            this.tbSpecHandicapOne = new System.Windows.Forms.TextBox();
            this.tbChildAge = new System.Windows.Forms.TextBox();
            this.tbChildName = new System.Windows.Forms.TextBox();
            this.cbSortHandicapThree = new System.Windows.Forms.ComboBox();
            this.cbSortHandicapTwo = new System.Windows.Forms.ComboBox();
            this.cbSortHandicapOne = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblChildNumber = new System.Windows.Forms.Label();
            this.btnSaveParent = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cbEdu = new System.Windows.Forms.ComboBox();
            this.cbWork = new System.Windows.Forms.ComboBox();
            this.cbSituation = new System.Windows.Forms.ComboBox();
            this.tbParentName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stateLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.vePanel.SuspendLayout();
            this.questionTab.SuspendLayout();
            this.profileTab.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(254, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vul hier uw vraag in";
            // 
            // tbQuestion
            // 
            this.tbQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbQuestion.Location = new System.Drawing.Point(35, 51);
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.Size = new System.Drawing.Size(686, 30);
            this.tbQuestion.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(617, 124);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(104, 36);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Zoeken";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // vePanel
            // 
            this.vePanel.Controls.Add(this.questionTab);
            this.vePanel.Controls.Add(this.profileTab);
            this.vePanel.Location = new System.Drawing.Point(12, 12);
            this.vePanel.Name = "vePanel";
            this.vePanel.SelectedIndex = 0;
            this.vePanel.Size = new System.Drawing.Size(757, 589);
            this.vePanel.TabIndex = 4;
            // 
            // questionTab
            // 
            this.questionTab.BackColor = System.Drawing.SystemColors.Control;
            this.questionTab.Controls.Add(this.cbChildThree);
            this.questionTab.Controls.Add(this.cbChildTwo);
            this.questionTab.Controls.Add(this.cbChildOne);
            this.questionTab.Controls.Add(this.btnSearch);
            this.questionTab.Controls.Add(this.label3);
            this.questionTab.Controls.Add(this.tbQuestion);
            this.questionTab.Controls.Add(this.label1);
            this.questionTab.Location = new System.Drawing.Point(4, 25);
            this.questionTab.Name = "questionTab";
            this.questionTab.Padding = new System.Windows.Forms.Padding(3);
            this.questionTab.Size = new System.Drawing.Size(749, 560);
            this.questionTab.TabIndex = 0;
            this.questionTab.Text = "Stel een vraag";
            // 
            // cbChildThree
            // 
            this.cbChildThree.AutoSize = true;
            this.cbChildThree.Location = new System.Drawing.Point(241, 87);
            this.cbChildThree.Name = "cbChildThree";
            this.cbChildThree.Size = new System.Drawing.Size(70, 21);
            this.cbChildThree.TabIndex = 8;
            this.cbChildThree.Text = "Kind 3";
            this.cbChildThree.UseVisualStyleBackColor = true;
            this.cbChildThree.Visible = false;
            this.cbChildThree.CheckedChanged += new System.EventHandler(this.cbChild_CheckedChanged);
            // 
            // cbChildTwo
            // 
            this.cbChildTwo.AutoSize = true;
            this.cbChildTwo.Location = new System.Drawing.Point(139, 87);
            this.cbChildTwo.Name = "cbChildTwo";
            this.cbChildTwo.Size = new System.Drawing.Size(70, 21);
            this.cbChildTwo.TabIndex = 7;
            this.cbChildTwo.Text = "Kind 2";
            this.cbChildTwo.UseVisualStyleBackColor = true;
            this.cbChildTwo.Visible = false;
            this.cbChildTwo.CheckedChanged += new System.EventHandler(this.cbChild_CheckedChanged);
            // 
            // cbChildOne
            // 
            this.cbChildOne.AutoSize = true;
            this.cbChildOne.Location = new System.Drawing.Point(35, 87);
            this.cbChildOne.Name = "cbChildOne";
            this.cbChildOne.Size = new System.Drawing.Size(70, 21);
            this.cbChildOne.TabIndex = 6;
            this.cbChildOne.Text = "Kind 1";
            this.cbChildOne.UseVisualStyleBackColor = true;
            this.cbChildOne.Visible = false;
            this.cbChildOne.CheckedChanged += new System.EventHandler(this.cbChild_CheckedChanged);
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(38, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(683, 2);
            this.label3.TabIndex = 5;
            // 
            // profileTab
            // 
            this.profileTab.BackColor = System.Drawing.SystemColors.Control;
            this.profileTab.Controls.Add(this.btnDelChild);
            this.profileTab.Controls.Add(this.btnDelAccount);
            this.profileTab.Controls.Add(this.lblBackupName);
            this.profileTab.Controls.Add(this.btnNewChild);
            this.profileTab.Controls.Add(this.lblFaultCodeProfile);
            this.profileTab.Controls.Add(this.label18);
            this.profileTab.Controls.Add(this.btnPreviousChild);
            this.profileTab.Controls.Add(this.btnNextChild);
            this.profileTab.Controls.Add(this.btnSaveChild);
            this.profileTab.Controls.Add(this.tbSpecHandicapThree);
            this.profileTab.Controls.Add(this.tbSpecHandicapTwo);
            this.profileTab.Controls.Add(this.tbSpecHandicapOne);
            this.profileTab.Controls.Add(this.tbChildAge);
            this.profileTab.Controls.Add(this.tbChildName);
            this.profileTab.Controls.Add(this.cbSortHandicapThree);
            this.profileTab.Controls.Add(this.cbSortHandicapTwo);
            this.profileTab.Controls.Add(this.cbSortHandicapOne);
            this.profileTab.Controls.Add(this.label17);
            this.profileTab.Controls.Add(this.label16);
            this.profileTab.Controls.Add(this.label15);
            this.profileTab.Controls.Add(this.label14);
            this.profileTab.Controls.Add(this.lblChildNumber);
            this.profileTab.Controls.Add(this.btnSaveParent);
            this.profileTab.Controls.Add(this.label12);
            this.profileTab.Controls.Add(this.cbEdu);
            this.profileTab.Controls.Add(this.cbWork);
            this.profileTab.Controls.Add(this.cbSituation);
            this.profileTab.Controls.Add(this.tbParentName);
            this.profileTab.Controls.Add(this.label11);
            this.profileTab.Controls.Add(this.label10);
            this.profileTab.Controls.Add(this.label9);
            this.profileTab.Controls.Add(this.label8);
            this.profileTab.Controls.Add(this.label7);
            this.profileTab.Controls.Add(this.label6);
            this.profileTab.Controls.Add(this.label5);
            this.profileTab.Controls.Add(this.label4);
            this.profileTab.Location = new System.Drawing.Point(4, 25);
            this.profileTab.Name = "profileTab";
            this.profileTab.Padding = new System.Windows.Forms.Padding(3);
            this.profileTab.Size = new System.Drawing.Size(749, 560);
            this.profileTab.TabIndex = 1;
            this.profileTab.Text = "Profiel";
            // 
            // btnDelChild
            // 
            this.btnDelChild.Location = new System.Drawing.Point(21, 438);
            this.btnDelChild.Name = "btnDelChild";
            this.btnDelChild.Size = new System.Drawing.Size(94, 43);
            this.btnDelChild.TabIndex = 40;
            this.btnDelChild.Text = "Verwijder kind";
            this.btnDelChild.UseVisualStyleBackColor = true;
            this.btnDelChild.Click += new System.EventHandler(this.btnDelChild_Click);
            // 
            // btnDelAccount
            // 
            this.btnDelAccount.Location = new System.Drawing.Point(596, 494);
            this.btnDelAccount.Name = "btnDelAccount";
            this.btnDelAccount.Size = new System.Drawing.Size(130, 54);
            this.btnDelAccount.TabIndex = 39;
            this.btnDelAccount.Text = "Verwijder account";
            this.btnDelAccount.UseVisualStyleBackColor = true;
            this.btnDelAccount.Click += new System.EventHandler(this.btnDelAccount_Click);
            // 
            // lblBackupName
            // 
            this.lblBackupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackupName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblBackupName.Location = new System.Drawing.Point(511, 497);
            this.lblBackupName.Name = "lblBackupName";
            this.lblBackupName.Size = new System.Drawing.Size(105, 28);
            this.lblBackupName.TabIndex = 38;
            this.lblBackupName.Visible = false;
            // 
            // btnNewChild
            // 
            this.btnNewChild.Location = new System.Drawing.Point(480, 438);
            this.btnNewChild.Name = "btnNewChild";
            this.btnNewChild.Size = new System.Drawing.Size(164, 31);
            this.btnNewChild.TabIndex = 37;
            this.btnNewChild.Text = "Voeg nieuw kind toe";
            this.btnNewChild.UseVisualStyleBackColor = true;
            this.btnNewChild.Click += new System.EventHandler(this.btnNewChild_Click);
            // 
            // lblFaultCodeProfile
            // 
            this.lblFaultCodeProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaultCodeProfile.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblFaultCodeProfile.Location = new System.Drawing.Point(18, 497);
            this.lblFaultCodeProfile.Name = "lblFaultCodeProfile";
            this.lblFaultCodeProfile.Size = new System.Drawing.Size(508, 28);
            this.lblFaultCodeProfile.TabIndex = 36;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(23, 484);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(703, 2);
            this.label18.TabIndex = 35;
            // 
            // btnPreviousChild
            // 
            this.btnPreviousChild.Location = new System.Drawing.Point(457, 394);
            this.btnPreviousChild.Name = "btnPreviousChild";
            this.btnPreviousChild.Size = new System.Drawing.Size(38, 38);
            this.btnPreviousChild.TabIndex = 34;
            this.btnPreviousChild.Text = "<<";
            this.btnPreviousChild.UseVisualStyleBackColor = true;
            this.btnPreviousChild.Click += new System.EventHandler(this.btnPreviousChild_Click);
            // 
            // btnNextChild
            // 
            this.btnNextChild.Location = new System.Drawing.Point(626, 394);
            this.btnNextChild.Name = "btnNextChild";
            this.btnNextChild.Size = new System.Drawing.Size(38, 38);
            this.btnNextChild.TabIndex = 33;
            this.btnNextChild.Text = ">>";
            this.btnNextChild.UseVisualStyleBackColor = true;
            this.btnNextChild.Click += new System.EventHandler(this.btnNextChild_Click);
            // 
            // btnSaveChild
            // 
            this.btnSaveChild.Location = new System.Drawing.Point(500, 394);
            this.btnSaveChild.Name = "btnSaveChild";
            this.btnSaveChild.Size = new System.Drawing.Size(120, 38);
            this.btnSaveChild.TabIndex = 32;
            this.btnSaveChild.Text = "Creëer Kind";
            this.btnSaveChild.UseVisualStyleBackColor = true;
            this.btnSaveChild.Click += new System.EventHandler(this.btnSaveChild_Click);
            // 
            // tbSpecHandicapThree
            // 
            this.tbSpecHandicapThree.Location = new System.Drawing.Point(618, 356);
            this.tbSpecHandicapThree.Name = "tbSpecHandicapThree";
            this.tbSpecHandicapThree.Size = new System.Drawing.Size(106, 22);
            this.tbSpecHandicapThree.TabIndex = 31;
            // 
            // tbSpecHandicapTwo
            // 
            this.tbSpecHandicapTwo.Location = new System.Drawing.Point(506, 356);
            this.tbSpecHandicapTwo.Name = "tbSpecHandicapTwo";
            this.tbSpecHandicapTwo.Size = new System.Drawing.Size(106, 22);
            this.tbSpecHandicapTwo.TabIndex = 30;
            // 
            // tbSpecHandicapOne
            // 
            this.tbSpecHandicapOne.Location = new System.Drawing.Point(394, 357);
            this.tbSpecHandicapOne.Name = "tbSpecHandicapOne";
            this.tbSpecHandicapOne.Size = new System.Drawing.Size(106, 22);
            this.tbSpecHandicapOne.TabIndex = 29;
            // 
            // tbChildAge
            // 
            this.tbChildAge.Location = new System.Drawing.Point(516, 293);
            this.tbChildAge.Name = "tbChildAge";
            this.tbChildAge.Size = new System.Drawing.Size(88, 22);
            this.tbChildAge.TabIndex = 28;
            // 
            // tbChildName
            // 
            this.tbChildName.Location = new System.Drawing.Point(470, 259);
            this.tbChildName.Name = "tbChildName";
            this.tbChildName.Size = new System.Drawing.Size(175, 22);
            this.tbChildName.TabIndex = 27;
            // 
            // cbSortHandicapThree
            // 
            this.cbSortHandicapThree.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSortHandicapThree.FormattingEnabled = true;
            this.cbSortHandicapThree.Location = new System.Drawing.Point(618, 325);
            this.cbSortHandicapThree.Name = "cbSortHandicapThree";
            this.cbSortHandicapThree.Size = new System.Drawing.Size(106, 24);
            this.cbSortHandicapThree.TabIndex = 26;
            // 
            // cbSortHandicapTwo
            // 
            this.cbSortHandicapTwo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSortHandicapTwo.FormattingEnabled = true;
            this.cbSortHandicapTwo.Location = new System.Drawing.Point(506, 325);
            this.cbSortHandicapTwo.Name = "cbSortHandicapTwo";
            this.cbSortHandicapTwo.Size = new System.Drawing.Size(106, 24);
            this.cbSortHandicapTwo.TabIndex = 25;
            // 
            // cbSortHandicapOne
            // 
            this.cbSortHandicapOne.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSortHandicapOne.FormattingEnabled = true;
            this.cbSortHandicapOne.Location = new System.Drawing.Point(394, 325);
            this.cbSortHandicapOne.Name = "cbSortHandicapOne";
            this.cbSortHandicapOne.Size = new System.Drawing.Size(106, 24);
            this.cbSortHandicapOne.TabIndex = 24;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(185, 357);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(165, 20);
            this.label17.TabIndex = 23;
            this.label17.Text = "Specifieke beperking";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(185, 325);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(165, 20);
            this.label16.TabIndex = 22;
            this.label16.Text = "Soort beperking";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(185, 294);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(165, 20);
            this.label15.TabIndex = 21;
            this.label15.Text = "Leeftijd van uw kind";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(185, 260);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(165, 20);
            this.label14.TabIndex = 20;
            this.label14.Text = "Naam van uw kind";
            // 
            // lblChildNumber
            // 
            this.lblChildNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChildNumber.ForeColor = System.Drawing.Color.IndianRed;
            this.lblChildNumber.Location = new System.Drawing.Point(36, 259);
            this.lblChildNumber.Name = "lblChildNumber";
            this.lblChildNumber.Size = new System.Drawing.Size(77, 30);
            this.lblChildNumber.TabIndex = 19;
            this.lblChildNumber.Text = "Kind 1";
            // 
            // btnSaveParent
            // 
            this.btnSaveParent.Location = new System.Drawing.Point(499, 202);
            this.btnSaveParent.Name = "btnSaveParent";
            this.btnSaveParent.Size = new System.Drawing.Size(112, 32);
            this.btnSaveParent.TabIndex = 18;
            this.btnSaveParent.Text = "Creëer ouder";
            this.btnSaveParent.UseVisualStyleBackColor = true;
            this.btnSaveParent.Click += new System.EventHandler(this.btnSaveParent_Click);
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(21, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(703, 2);
            this.label12.TabIndex = 17;
            // 
            // cbEdu
            // 
            this.cbEdu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEdu.FormattingEnabled = true;
            this.cbEdu.Items.AddRange(new object[] {
            "",
            "VMBO",
            "HAVO",
            "VWO",
            "MBO",
            "HBO",
            "WO"});
            this.cbEdu.Location = new System.Drawing.Point(465, 172);
            this.cbEdu.Name = "cbEdu";
            this.cbEdu.Size = new System.Drawing.Size(175, 24);
            this.cbEdu.TabIndex = 16;
            // 
            // cbWork
            // 
            this.cbWork.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWork.FormattingEnabled = true;
            this.cbWork.Items.AddRange(new object[] {
            "",
            "Voltijd (36-40 uur)",
            "Deeltijd (<36 uur)",
            "Werkzoekende"});
            this.cbWork.Location = new System.Drawing.Point(465, 142);
            this.cbWork.Name = "cbWork";
            this.cbWork.Size = new System.Drawing.Size(175, 24);
            this.cbWork.TabIndex = 15;
            // 
            // cbSituation
            // 
            this.cbSituation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSituation.FormattingEnabled = true;
            this.cbSituation.Items.AddRange(new object[] {
            "",
            "Alleenstaand",
            "Getrouwd"});
            this.cbSituation.Location = new System.Drawing.Point(465, 112);
            this.cbSituation.Name = "cbSituation";
            this.cbSituation.Size = new System.Drawing.Size(175, 24);
            this.cbSituation.TabIndex = 14;
            // 
            // tbParentName
            // 
            this.tbParentName.Location = new System.Drawing.Point(465, 84);
            this.tbParentName.Name = "tbParentName";
            this.tbParentName.Size = new System.Drawing.Size(175, 22);
            this.tbParentName.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(185, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(219, 43);
            this.label11.TabIndex = 12;
            this.label11.Text = "Hoogst genoten opleiding";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(185, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 16);
            this.label10.TabIndex = 11;
            this.label10.Text = "Werksituatie";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(185, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Leefsituatie";
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(121, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(2, 415);
            this.label8.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(185, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 22);
            this.label7.TabIndex = 8;
            this.label7.Text = "Je gebruikersnaam";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.IndianRed;
            this.label6.Location = new System.Drawing.Point(36, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 30);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ouder";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(21, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(703, 2);
            this.label5.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(341, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 30);
            this.label4.TabIndex = 1;
            this.label4.Text = "Profiel";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stateLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 588);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(781, 25);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stateLabel
            // 
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(54, 20);
            this.stateLabel.Text = "<text>";
            // 
            // VoorElkaar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 613);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.vePanel);
            this.Name = "VoorElkaar";
            this.Text = "Voor Elkaar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VoorElkaar_FormClosing);
            this.Load += new System.EventHandler(this.voorElkaar_Load);
            this.vePanel.ResumeLayout(false);
            this.questionTab.ResumeLayout(false);
            this.questionTab.PerformLayout();
            this.profileTab.ResumeLayout(false);
            this.profileTab.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TabControl vePanel;
        private System.Windows.Forms.TabPage questionTab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage profileTab;
        private System.Windows.Forms.ComboBox cbEdu;
        private System.Windows.Forms.ComboBox cbWork;
        private System.Windows.Forms.ComboBox cbSituation;
        private System.Windows.Forms.TextBox tbParentName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbChildAge;
        private System.Windows.Forms.TextBox tbChildName;
        private System.Windows.Forms.ComboBox cbSortHandicapThree;
        private System.Windows.Forms.ComboBox cbSortHandicapTwo;
        private System.Windows.Forms.ComboBox cbSortHandicapOne;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblChildNumber;
        private System.Windows.Forms.Button btnSaveParent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnPreviousChild;
        private System.Windows.Forms.Button btnNextChild;
        private System.Windows.Forms.Button btnSaveChild;
        private System.Windows.Forms.TextBox tbSpecHandicapThree;
        private System.Windows.Forms.TextBox tbSpecHandicapTwo;
        private System.Windows.Forms.TextBox tbSpecHandicapOne;
        private System.Windows.Forms.Label lblFaultCodeProfile;
        private System.Windows.Forms.Button btnNewChild;
        private System.Windows.Forms.Label lblBackupName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel stateLabel;
        private System.Windows.Forms.Button btnDelChild;
        private System.Windows.Forms.Button btnDelAccount;
        private System.Windows.Forms.CheckBox cbChildThree;
        private System.Windows.Forms.CheckBox cbChildTwo;
        private System.Windows.Forms.CheckBox cbChildOne;
    }
}

