﻿using Schouders.BU;
using Schouders.CC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoorElkaarv2
{

    public partial class VoorElkaar : Form
    {
        private List<Child> children = new List<Child>();
        private List<CheckBox> boxList = new List<CheckBox>();

        private int i = 0;
        private State parentState;
        private State childState;

        #region -- Constructor method(s)
        public VoorElkaar()
        {
            InitializeComponent();
        }
        #endregion

        #region -- Startup methods
        // 
        private void voorElkaar_Load(object sender, EventArgs e)
        {
            // Fill the boxList list.
            boxList.Add(cbChildOne);
            boxList.Add(cbChildTwo);
            boxList.Add(cbChildThree);

            // Get the correct states from the settings
            parentState = (State)Properties.Settings.Default.parentState;
            childState = (State)Properties.Settings.Default.childState;

            // Get the limitations
            this.retrieveLimitations();

            // Check if the user was still adding the parent's data when closing.
            Console.WriteLine($"{parentState.Equals("").ToString()} + {parentState.Equals("Creating").ToString()}");
            if (parentState == State.Creating)
            {
                vePanel.SelectedTab = profileTab;
                childState = State.Creating;
                questionTab.Enabled = false;
                btnDelChild.Visible = false;
                btnDelAccount.Visible = false;
            }
            else
            {
                // Get the parent data;
                this.retrieveParentData();

                // Check if the user was done adding the child.
                Console.WriteLine($"{Properties.Settings.Default.childState.ToString().Equals("Updating").ToString()} + {Properties.Settings.Default.childState.ToString().Equals("Adding").ToString()}");
                if (childState == State.Updating || childState == State.Adding)
                {
                    // Retrieve the children
                    this.retrieveChildren();

                    // Show them to the user.
                    this.setSelectedChild();

                    // Set the checkboxes in the search window.
                    this.setCheckboxNames();

                    // Set the environment details.
                    parentState = State.Updating;
                    questionTab.Enabled = true;
                    btnDelChild.Visible = true;
                }
                else
                {
                    questionTab.Enabled = false;
                    btnDelAccount.Visible = true;
                    btnDelChild.Visible = false;
                }
            }
        }

        // Gets the children's data and sets the environment settings.
        private void retrieveChildren()
        {
            // Get the parent's name to retrieve the parentID.
            string pName = tbParentName.Text;

            // Get the ID to add the child to the correct parent.
            int id = ccRetrieveData.getParentID(pName);
            children = ccRetrieveData.retrieveChild(id);

            // Establish the correct environment changed based on the retrieval.
            // Can ask questions, will update child.
            btnSaveChild.Text = "Update kind";
            tbChildName.Enabled = false;
        }

        // Gets the parent's data and sets the environment settings.
        private void retrieveParentData()
        {
            // Get the name so that you can look if the parent exists.
            string pName = Properties.Settings.Default.name;
            lblBackupName.Text = pName;
            if (!ccRetrieveData.doesParentExist(pName)) return;

            // Set the correct environment values.
            btnSaveParent.Text = "Update Parent";
            tbParentName.Enabled = false;

            // Retrieve the actual parent from the database.
            Parent p = ccRetrieveData.retrieveParent(pName);

            // Add the data to the form.
            tbParentName.Text = p.Name;
            cbSituation.SelectedItem = p.Situation;
            cbWork.SelectedItem = p.Job;
            cbEdu.SelectedItem = p.Education;
        }

        // Get the limitations to add to the comboboxes.
        private void retrieveLimitations()
        {
            Dictionary<int, string> limitations = ccRetrieveData.getLimitations();

            foreach (var dic in limitations)
            {
                cbSortHandicapOne.Items.Add(limitations[dic.Key]);
                cbSortHandicapTwo.Items.Add(limitations[dic.Key]);
                cbSortHandicapThree.Items.Add(limitations[dic.Key]);
            }
            // Set the default selection to the 'no handicap'-option to prevent crashes.
            cbSortHandicapOne.SelectedIndex = 0;
            cbSortHandicapTwo.SelectedIndex = 0;
            cbSortHandicapThree.SelectedIndex = 0;
        }
        #endregion

        #region -- Parent methods
        // The method that's fired when the user presses the 'save/update parent'-button.
        private void btnSaveParent_Click(object sender, EventArgs e)
        {
            // Check if the fields are filled properly.
            if (!this.checkParentData()) return;

            // Check if the user is creating or updating the fields.
            if (parentState == State.Creating)
            {
                if (!this.createParent())
                {
                    stateLabel.ForeColor = Color.Red;
                    stateLabel.Text = "De ouder kon niet aangemaakt worden";
                    return;
                }
                // Set the environmental values.
                stateLabel.ForeColor = Color.Green;
                stateLabel.Text = "De ouder is aangemaakt";
                tbParentName.Enabled = false;
                parentState = State.Updating;
                btnDelAccount.Visible = true;
            }
            else if (parentState == State.Updating)
            {
                if (!this.updateParent())
                {
                    stateLabel.ForeColor = Color.Red;
                    stateLabel.Text = "De ouder kon niet geupdatet worden";
                    return;
                }
                stateLabel.ForeColor = Color.Green;
                stateLabel.Text = "De ouder is geupdatet";

            }
        }
        // The method that updates the parent in the database.
        private bool updateParent() {
            string pName = tbParentName.Text;
            string situation = cbSituation.SelectedItem.ToString();
            string work = cbWork.SelectedItem.ToString();
            string edu = cbEdu.SelectedItem.ToString();

            bool isUpdated = ccProfileManagement.updateProfileParent(pName, situation, work, edu);
            lblBackupName.Text = pName;
            return isUpdated;
        }

        // The method that checks if the parent data will not cause issues.
        private bool checkParentData()
        {
            string pName = tbParentName.Text;
            if (pName.Equals(""))
            {
                lblFaultCodeProfile.Text = "Er is geen naam ingevuld!";
                return false;
            }
            else if (pName.Length <= 2)
            {
                lblFaultCodeProfile.Text = "De naam die u hebt ingevuld is te kort.";
                return false;
            }

            if (parentState == State.Creating && ccRetrieveData.doesParentExist(pName))
            {
                lblFaultCodeProfile.Text = "Er bestaat al een gebruiker met deze naam.";
                return false;
            }
            return true;
        }

        // The method that creates the parent in the database.
        private bool createParent()
        {
            // Get the data
            string pName = tbParentName.Text;
            string situation = cbSituation.SelectedItem.ToString();
            string work = cbWork.SelectedItem.ToString();
            string edu = cbEdu.SelectedItem.ToString();

            // Create the parent
            bool isCreated = ccProfileManagement.createProfileParent(pName, situation, work, edu);
            lblBackupName.Text = pName;

            // return whether the parent has been created.
            return isCreated;

        }

        // Deleting the account from the database.
        private void btnDelAccount_Click(object sender, EventArgs e)
        {
            // Delete the account
            string pName = tbParentName.Text;
            ccProfileManagement.deleteAccount(pName);

            // Set the form again.
            childState = State.Creating;
            parentState = State.Creating;

            tbParentName.Enabled = true;
            tbChildName.Enabled = true;

            // reset the entire profile form
            tbParentName.Text = "";
            tbSpecHandicapOne.Text = "";
            tbSpecHandicapTwo.Text = "";
            tbSpecHandicapThree.Text = "";
            tbChildAge.Text = "";
            tbChildName.Text = "";

            cbSituation.SelectedIndex = 0;
            cbEdu.SelectedIndex = 0;
            cbWork.SelectedIndex = 0;
            cbSortHandicapOne.SelectedIndex = 0;
            cbSortHandicapThree.SelectedIndex = 0;
            cbSortHandicapTwo.SelectedIndex = 0;

            // Variables
            children = new List<Child>();
            btnDelAccount.Visible = false;
            btnDelChild.Visible = false;

            // Set the ask question tab disabled
            questionTab.Enabled = false;

            // Set the labels
            btnSaveChild.Text = "Creeër kind";
            btnSaveParent.Text = "Creeër ouder";



        }
        #endregion

        #region -- Child methods
        // The event that triggers when the save button is hit.
        private void btnSaveChild_Click(object sender, EventArgs e)
        {
            // Check the data including the parentName.
            if (!checkChildData()) return;

            // Send it to the CC layer to create the child.
            if (childState == State.Creating || childState == State.Adding)
            {
                if (!this.createChild())
                {
                    stateLabel.ForeColor = Color.Red;
                    stateLabel.Text = "Het kind kon niet aangemaakt worden";
                    return;
                }
                stateLabel.ForeColor = Color.Green;
                stateLabel.Text = "Het kind is aangemaakt";
                // Disable the textbox
                tbChildName.Enabled = false;
                childState = State.Updating;
                btnDelChild.Visible = true;
            }
            else if (childState == State.Updating)
            {
                if (!this.updateChild())
                {
                    stateLabel.ForeColor = Color.Red;
                    stateLabel.Text = "Het kind kon niet geupdatet worden";
                    return;
                }
                stateLabel.ForeColor = Color.Green;
                stateLabel.Text = "Het kind is geupdatet";
                // Disable the textbox
                tbChildName.Enabled = false;             
                childState = State.Updating;
            }
        }

        // The method that is directly responsible for updating the child.
        private bool updateChild()
        {
            string pName = tbParentName.Text;
            string cName = tbChildName.Text;
            int cAge = Convert.ToInt32(tbChildAge.Text);
            string sortHandicapOne = cbSortHandicapOne.SelectedItem.ToString();
            string sortHandicapTwo = cbSortHandicapTwo.SelectedItem.ToString();
            string sortHandicapThree = cbSortHandicapThree.SelectedItem.ToString();
            string specHandicapOne = tbSpecHandicapOne.Text;
            string specHandicapTwo = tbSpecHandicapTwo.Text;
            string specHandicapThree = tbSpecHandicapThree.Text;

            bool isUpdated = ccProfileManagement.updateProfileChild(pName, cName, cAge, sortHandicapOne,
                                            specHandicapOne, sortHandicapTwo, specHandicapTwo,
                                            sortHandicapThree, specHandicapThree);
            // Update the list of children in memory.
            this.retrieveChildren();

            // Set the checkboxes in the search window.
            this.setCheckboxNames();

            return isUpdated;
        }

        // The method that is directly responsible for creating the child.
        private bool createChild()
        {
            // Get the data.
            string pName = tbParentName.Text;
            string cName = tbChildName.Text;
            int cAge = Convert.ToInt32(tbChildAge.Text);
            string sortHandicapOne = cbSortHandicapOne.SelectedItem.ToString();
            string sortHandicapTwo = cbSortHandicapTwo.SelectedItem.ToString();
            string sortHandicapThree = cbSortHandicapThree.SelectedItem.ToString();
            string specHandicapOne = tbSpecHandicapOne.Text;
            string specHandicapTwo = tbSpecHandicapTwo.Text;
            string specHandicapThree = tbSpecHandicapThree.Text;

            // Create the child.
            bool isCreated = ccProfileManagement.createProfileChild(pName, cName, cAge, sortHandicapOne,
                                            specHandicapOne, sortHandicapTwo, specHandicapTwo,
                                            sortHandicapThree, specHandicapThree);

            // Update the list of children in memory.
            this.retrieveChildren();

            // Set the checkboxes in the search window.
            this.setCheckboxNames();

            // 
            if (children.Count > 0)
            {
                questionTab.Enabled = true;
            }

            // Return if the child was created.
            return isCreated;
            
        }

        // The method that checks the child data.
        private bool checkChildData()
        {
            // Check if the parent's name is filled.
            if (tbParentName.Text.Equals(""))
            {
                lblFaultCodeProfile.Text = "Er is geen ouder ingevuld";
                return false;
            }

            // Check if at least one handicap is filled.
            if (tbChildName.Text.Equals("") ||
                cbSortHandicapOne.SelectedItem.ToString().Equals("") ||
                tbSpecHandicapOne.Text.Equals(""))
            {
                stateLabel.Text = "Het kind kon niet worden geüpdatet omdat er geen beperking is ingevuld.";
                return false;
            }
            return true;
        }

        // The button to select the next child.
        private void btnNextChild_Click(object sender, EventArgs e)
        {
            if (i >= children.Count - 1) i = 0;
            else i++;
            this.setSelectedChild();

        }

        // The method that makes sure the child data is shown properly
        private void setSelectedChild()
        {
            // Loads all data from the list to the form.
            if (children == null) return;
            if (children.Count == 0) return;
            tbChildName.Text = children[i].Name;
            tbChildAge.Text = children[i].Age.ToString();
            cbSortHandicapOne.SelectedItem = children[i].SortHandicapOne;
            cbSortHandicapTwo.SelectedItem = children[i].SortHandicapTwo;
            cbSortHandicapThree.SelectedItem = children[i].SortHandicapThree;
            tbSpecHandicapOne.Text = children[i].SpecHandicapOne;
            tbSpecHandicapTwo.Text = children[i].SpecHandicapTwo;
            tbSpecHandicapThree.Text = children[i].SpecHandicapThree;
            tbChildName.Enabled = false;
            childState = State.Updating;
            lblChildNumber.Text = $"Kind {i + 1}";
        }

        // The button to select the previous child.
        private void btnPreviousChild_Click(object sender, EventArgs e)
        {
            if (children == null) return;
            if (i <= 0) i = children.Count - 1;
            else i--;
            this.setSelectedChild();
        }

        // The button to create a new child.
        private void btnNewChild_Click(object sender, EventArgs e)
        {
            // Tell the system it currectly is adding.
            childState = State.Adding;

            // Reset the Child form controls.
            tbChildName.Text = "";
            tbChildAge.Text = "";
            tbSpecHandicapOne.Text = "";
            tbSpecHandicapTwo.Text = "";
            tbSpecHandicapThree.Text = "";
            cbSortHandicapOne.SelectedIndex = 0;
            cbSortHandicapTwo.SelectedIndex = 0;
            cbSortHandicapThree.SelectedIndex = 0;

            btnSaveChild.Text = "Aanmaken kind";

            // Let the user be able to add a name to the textbox.
            tbChildName.Enabled = true;
        }

        // The button to delete a child.
        private void btnDelChild_Click(object sender, EventArgs e)
        {
            if (childState == State.Updating && children.Count != 1)
            {
                string pName = tbParentName.Text;
                string cName = tbChildName.Text;

                ccProfileManagement.deleteProfileChild(pName, cName);
                this.retrieveChildren();
                // Set the checkboxes in the search window.
                this.setCheckboxNames();
            }
            else
            {
                stateLabel.Text = "Ieder account moet minimaal één kind hebben";
            }
        }
        #endregion

        #region -- Closing methods
        // Make sure to save the parent's name so you can get it again if he opens it again.
        private void VoorElkaar_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.name = lblBackupName.Text;
            Properties.Settings.Default.childState = (int) childState;

            if (!tbParentName.Enabled) Properties.Settings.Default.parentState = (int)State.Updating; 
            else Properties.Settings.Default.parentState = (int)State.Creating;

            Properties.Settings.Default.Save();

            Console.WriteLine($"name: {Properties.Settings.Default.name} + " +
                               $"parentState: {Properties.Settings.Default.parentState} + " +
                               $"childState): {Properties.Settings.Default.childState} + ");
        }
        #endregion

        #region -- Search Window
        // The button that is pressed when a question is being submitted.
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!searchChecks()) return;
            stateLabel.Text = "Aan het zoeken...";
            string question = tbQuestion.Text;
            string pName = tbParentName.Text;
            string cName = "";

            foreach(var box in boxList)
                if (box.Checked) cName = box.Text;

            ccAskQuestion.AskQuestion(question, pName, cName);
        }

        // The checks done on the question.
        private bool searchChecks()
        {
            if (tbQuestion.Text.Length < 10)
            {
                return false;
            }
            return true;
        }

        // Make sure the user cannot search for more than 1 child.
        private void cbChild_CheckedChanged(object sender, EventArgs e)
        {

            // Get the box that has been clicked.
            CheckBox clickedBox = sender as CheckBox;

            // Set the other children unselected.
            if (clickedBox.Checked)
                foreach(var box in boxList)
                    if (!box.Name.Equals(clickedBox.Name))
                        box.Checked = false;
        }

        // Sets the checkbox names of the search criteria.
        private void setCheckboxNames()
        {
            foreach (var box in boxList)
                box.Visible = false;

            for (int i = 0; i < Math.Min(children.Count, 3); i++)
            {
                boxList[i].Text = children[i].Name;
                boxList[i].Visible = true;
            }
        }
        #endregion

    }
}
