﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoorElkaarv2
{
    public enum State
    {
        Creating = 0,
        Updating = 1,
        Adding = 2,
    }
}
