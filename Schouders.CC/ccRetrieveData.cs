﻿using Schouders.BU;
using Schouders.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.CC
{
    public static class ccRetrieveData
    {
        private static Dictionary<int, string> handicaps;

        #region -- Limitations
        public static int getLimitationID(string limitationName)
        {
            DataTable dt;

            dt = InternalDB.readDatabase($"SELECT limitationID, name FROM LIMITATION " +
                                $"WHERE name='{limitationName}'");

            return (int)dt.Rows[0][0];
        }
        public static Dictionary<int, string> getLimitations()
        {
            handicaps = new Dictionary<int, string>();

            DataTable dt = InternalDB.readDatabase($"SELECT limitationID, name FROM LIMITATION");

            for (int i = 0; i < dt.Rows.Count; i++)
                handicaps.Add((int)dt.Rows[i][0], (string)dt.Rows[i][1]);

            return handicaps;
        }
        #endregion

        #region -- Parent
        public static int getParentID(string parentName)
        {
            DataTable dt = InternalDB.readDatabase($"SELECT parentID FROM PARENT " +
                                                $"WHERE name='{parentName}'");

            return (int)dt.Rows[0][0];
        }
        public static bool doesParentExist(string parentName)
        {
            DataTable dt = InternalDB.readDatabase($"SELECT * FROM PARENT " +
                                                $"WHERE name='{parentName}'");

            if (dt.Rows.Count == 0) return false;
            return true;
        }
        public static Parent retrieveParent(string parentName)
        {

            DataTable dt = InternalDB.readDatabase($"SELECT * FROM PARENT " +
                                $"WHERE name='{parentName}'");

            int parentID = (int)dt.Rows[0]["parentID"];
            string name = (string)dt.Rows[0]["name"];
            string situation = (string)dt.Rows[0]["situation"];
            string work = (string)dt.Rows[0]["work"];
            string edu = (string)dt.Rows[0]["education"];

            Parent parent = new Parent(parentID, name, situation, work, edu);
            return parent;
        }
        #endregion

        #region -- Child
        public static List<Child> retrieveChild(int parentID)
        {
            // Query
            DataTable dt = InternalDB.readDatabase($"SELECT * FROM CHILD " +
                                $"WHERE parentID={parentID}");

            if (dt.Rows.Count == 0) return null;

            int count = dt.Rows.Count;

            List<Child> children = new List<Child>();

            // Set the data from the datatable.
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                children.Add(CreateChild(dt, i));
            };

            return children;
        }

        // Create an child object that can be sent back to the GUI.
        private static Child CreateChild(DataTable dt, int i)
        {
            string childName = (string)dt.Rows[i]["name"];
            int childAge = (int)dt.Rows[i]["age"];
            string soh1;
            handicaps.TryGetValue((int)dt.Rows[i]["sortHandicapOne"], out soh1);
            string sph1 = (string)dt.Rows[i]["specHandicapOne"];
            string soh2;
            handicaps.TryGetValue((int)dt.Rows[i]["sortHandicapTwo"], out soh2);
            string sph2 = (string)dt.Rows[i]["specHandicapTwo"];
            string soh3;
            handicaps.TryGetValue((int)dt.Rows[i]["sortHandicapThree"], out soh3);
            string sph3 = (string)dt.Rows[i]["specHandicapThree"];

            // Create the child with this data
            Child child = new Child(childName, childAge, soh1, sph1, soh2, sph2, soh3, sph3);
            return child;
        }
        #endregion 
    }
}
