﻿using Schouders.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schouders.CC
{
    public static class ccAskQuestion
    {        
        public static string AskQuestion(string question,
                                  string parentName,
                                  string childName)
        {
            Console.WriteLine($"question: {question} \n" +
                              $"parentName: {parentName} \n" +
                              $"childName: {childName}");

            //// Steps to finish this CC:
            // 1. Check if question has been asked already.
            // 2. Get answer to this question
            // 3. Create instance of History with data retrieved.
            // 4. Save it to the database
            string query = "";
            InternalDB.modifyDatabase(query);
            // 5. Send the data back to the user.
            return "";
        }
    }
}
