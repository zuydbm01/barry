﻿using Schouders.BU;
using Schouders.DAL;
using System.Collections.Generic;
using System.Data;

namespace Schouders.CC
{
    public static class ccProfileManagement
    {
        #region -- Parent management
        // Create the profile of the parent.
        public static bool createProfileParent(string name, 
                                         string situation,
                                         string job,
                                         string education)
        {

            Parent parent = new Parent(name, situation, job, education);
            string query = $"INSERT INTO PARENT" +
                           $"(name, situation, work, education) " +
                           $"VALUES(" +
                           $"'{name}', '{situation}', '{job}', '{education}')";

            // Add Parent to database.
            return InternalDB.modifyDatabase(query);
        }

        // Updates the profile of the parent.
        public static bool updateProfileParent(string pName, 
                                        string situation, 
                                        string work, 
                                        string edu)
        {

            Parent parent = new Parent(pName, situation, work, edu);
            string query = $"UPDATE PARENT " +
                           $"SET situation='{situation}', work='{work}', education='{edu}' " +
                           $"WHERE name='{pName}'";

            // Add Parent to database.
            return InternalDB.modifyDatabase(query);

        }
        #endregion

        #region -- Child Management
        // Create the profile of the child.
        public static bool createProfileChild(string parentName,
                                        string childName,
                                        int age,
                                        string sortHandicapOne,
                                        string specHandicapOne,
                                        string sortHandicapTwo = "",
                                        string specHandicapTwo = "",
                                        string sortHandicapThree = "",
                                        string specHandicapThree = "")
        {

            // Create a child object.
            Child child = new Child(childName,
                                    age,
                                    sortHandicapOne,
                                    specHandicapOne,
                                    sortHandicapTwo,
                                    specHandicapTwo,
                                    sortHandicapThree,
                                    specHandicapThree);

            // Get the id of the parent
            int parentID = ccRetrieveData.getParentID(parentName);
            List<int> handicapIDs = new List<int>();
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapOne));
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapTwo));
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapThree));

            string query = $"INSERT INTO CHILD " +
                            $"(parentID, name, age, sortHandicapOne, specHandicapOne, " +
                            $"sortHandicapTwo, specHandicapTwo, sortHandicapThree, specHandicapThree)" +
                            $" VALUES( '{parentID}', '{childName}', {age}, '{handicapIDs[0]}', " +
                            $"'{specHandicapOne}', '{handicapIDs[1]}', '{specHandicapTwo}', '{handicapIDs[2]}', '{specHandicapThree}')";

            // Add Child to the database.
            return InternalDB.modifyDatabase(query);
        }

        // Updates the profile of the parent.
        public static bool updateProfileChild(string parentName,
                                        string childName,
                                        int age,
                                        string sortHandicapOne,
                                        string specHandicapOne,
                                        string sortHandicapTwo = "",
                                        string specHandicapTwo = "",
                                        string sortHandicapThree = "",
                                        string specHandicapThree = "")
        {

            // Create a child object.
            Child child = new Child(childName,
                                    age,
                                    sortHandicapOne,
                                    specHandicapOne,
                                    sortHandicapTwo,
                                    specHandicapTwo,
                                    sortHandicapThree,
                                    specHandicapThree);

            // Get the id of the parent
            int parentID = ccRetrieveData.getParentID(parentName);
            List<int> handicapIDs = new List<int>();
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapOne));
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapTwo));
            handicapIDs.Add(ccRetrieveData.getLimitationID(sortHandicapThree));

            string query = $"UPDATE CHILD SET " +
                            $"age={age}, sortHandicapOne={handicapIDs[0]}, sortHandicapTwo={handicapIDs[1]}, " +
                            $"sortHandicapThree={handicapIDs[2]}, specHandicapOne='{specHandicapOne}', " +
                            $"specHandicapTwo='{specHandicapTwo}', specHandicapThree='{specHandicapThree}'" +
                            $"WHERE parentID={parentID} AND name='{childName}'";

            // Add Child to the database.
            return InternalDB.modifyDatabase(query);
        }

        public static bool deleteProfileChild(string parentName, string childName)
        {
            // Get the id of the parent
            int parentID = ccRetrieveData.getParentID(parentName);
            
            string query = $"DELETE FROM CHILD " +
                           $"WHERE parentID={parentID} AND name='{childName}'";

            return InternalDB.modifyDatabase(query);
        }

        public static bool deleteAccount(string parentName)
        {
            // Get the id of the parent
            int parentID = ccRetrieveData.getParentID(parentName);

            string childQ = "DELETE FROM CHILD " +
                            $"WHERE parentID={parentID}";

            string parentQ = "DELETE FROM PARENT " +
                            $"WHERE parentID={parentID}";

            InternalDB.modifyDatabase(childQ);
            InternalDB.modifyDatabase(parentQ);


            return false;
        }
        #endregion 
    }
}
